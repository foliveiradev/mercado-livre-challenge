package com.github.oliveiradev.mercadolivrechallenge

import android.app.Application

class MercadoLivreApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startLeakCanary()
        meliStarKoin()
        startTimber()
    }
}