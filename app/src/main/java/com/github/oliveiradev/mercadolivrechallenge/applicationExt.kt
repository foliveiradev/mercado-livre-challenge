package com.github.oliveiradev.mercadolivrechallenge

import android.app.Application
import com.github.oliveiradev.core.data.injection.dataModule
import com.github.oliveiradev.core.injection.coreModule
import com.github.oliveiradev.search.injection.searchModule
import com.squareup.leakcanary.LeakCanary
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

fun Application.startLeakCanary() {
    if (LeakCanary.isInAnalyzerProcess(this)) {
        return
    }

    LeakCanary.install(this)
}

fun Application.meliStarKoin() {
    startKoin {
        androidContext(this@meliStarKoin)
        modules(listOf(dataModule, coreModule, searchModule))
    }
}

fun startTimber() {
    if (BuildConfig.DEBUG)
        Timber.plant(Timber.DebugTree())
}