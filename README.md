# Mercado Livre Challenge

[![Build Status](https://app.bitrise.io/app/53cd0cd49e8dd3ce/status.svg?token=QbnGoSdrihOx2CdDOiqcUA&branch=master)](https://app.bitrise.io/app/53cd0cd49e8dd3ce)
[![Coverage Status](https://coveralls.io/repos/bitbucket/foliveiradev/mercado-livre-challenge/badge.svg?branch=feature/searc_product)](https://coveralls.io/bitbucket/foliveiradev/mercado-livre-challenge?branch=feature/searc_product)

> Desafio prático relacionado ao processo seletivo do Mercado Livre

# Decisões tomadas

* Separação em módulos. Isso permite isolar alguns contextos, nesse caso específico, a camada de infraestrutura ficou separada da parte visual da feature. Outro ponto importante(em projetos maiores) o tempo de build melhora quando dividimos em módulos, um dos motivos é a compilação incremental usada pelo gradle, entre outros motivos.
* Não fazer a chamada a API para buscar os detalhes de um item. O item é passado da lista para o detalhe, com isso, perdemos um pouco da riqueza dos dados, como por exemplo mais fotos do produto. Desta forma, também não poderiamos abrir o detalhe por deeplink.
* Foi usado alguns dos Architecture Components. Mais especificamente ViewModel(péssimo nome) e o LiveData, o ViewModel foi usado com inteção de facilitar na retenção do estado nas rotações de tela e o LiveData para ter um data holder que tem conhecimento de ciclo de vida evitando notificações em estados inválidos.
* Foi usado jacoco para cobertura de teste. Foi usado também para facilitar a geração de um relatório unificado(já que o app foi dividido em módulos)
* Foi usado Koin para facilitar na inversão de controle(koin usa service locator, não DI propriamente). 
* Foi usado Coroutines para execução de tarefas assíncronas e para mudanças de contexto de forma fácil(ex: IO para Main thread).

# Ferramentas

* Jacoco - cobertura de código
* Ktlint - Análise estática
* Leak canary - Detecção de leaks de memória

# Poderia ser melhor

* Os testes de espresso não usam idle resources ou qualquer estratégia do tipo, isso impacta em como a classe `SearchProductActivity` foi testada, em vários momentos foi necessário usar sleep nos cenários
* Existe uma pequena integração de CI/CD com Bitrise, mas apenas a task de rodar o ktlint foi feita.

# O APP não tem

* Tradução para outros idiomas
* Minificação/ofuscação de código e assets
* Paginação da lista do produto buscado