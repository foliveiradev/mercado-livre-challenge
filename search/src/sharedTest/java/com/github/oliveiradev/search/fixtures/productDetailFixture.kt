package com.github.oliveiradev.search.fixtures

import com.github.oliveiradev.core.data.search.InstallmentsResponse
import com.github.oliveiradev.core.data.search.PagingResponse
import com.github.oliveiradev.core.data.search.SearchItemResponse
import com.github.oliveiradev.core.data.search.SearchResponse
import com.github.oliveiradev.core.data.search.ShippingResponse
import com.github.oliveiradev.search.presentation.ProductScreenModel
import com.github.oliveiradev.search.presentation.SearchScreenModel

fun createProductScreenModel(isFreeShipping: Boolean): ProductScreenModel {
    return ProductScreenModel(
        "mesa",
        "R$ 100",
        "12x R$ 10",
        "http://mlu-s2-p.mlstatic.com/676307-MLU25783605634_072017-I.jpg",
        "novo",
        "50",
        "25",
        isFreeShipping
    )
}

fun createSearchResponse(items: List<SearchItemResponse> = emptyList()): SearchResponse {
    return SearchResponse(
        "query",
        "siteId",
        PagingResponse(1, 0, 50),
        items
    )
}

fun createSearchItemResponse(
    size: Int,
    installmentsResponse: InstallmentsResponse? = null
): List<SearchItemResponse> {
    return (0 until size).map {
        SearchItemResponse(
            "title",
            "thumbnail",
            100.toBigDecimal(),
            "BRL",
            installmentsResponse,
            "new",
            10,
            5,
            ShippingResponse(true)
        )
    }
}

fun createSearchItemResponse(installmentsResponse: InstallmentsResponse?): SearchItemResponse {
    return createSearchItemResponse(1, installmentsResponse).first()
}

fun createInstallmentResponse(): InstallmentsResponse {
    return InstallmentsResponse(10, 10.toBigDecimal(), 0, "BRL")
}

fun createSearchScreenModel(): SearchScreenModel {
    return SearchScreenModel(
        "1000",
        (0 until 2).map { createProductScreenModel(true) }
    )
}