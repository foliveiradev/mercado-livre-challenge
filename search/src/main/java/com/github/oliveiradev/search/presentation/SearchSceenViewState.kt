package com.github.oliveiradev.search.presentation

sealed class SearchScreenViewState

object Loading : SearchScreenViewState()
object NoContent : SearchScreenViewState()
data class Error(val error: Throwable) : SearchScreenViewState()
data class Content(val content: SearchScreenModel) : SearchScreenViewState()