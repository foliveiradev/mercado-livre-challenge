package com.github.oliveiradev.search.mapper

import com.github.oliveiradev.core.data.search.InstallmentsResponse
import com.github.oliveiradev.core.data.search.SearchItemResponse
import com.github.oliveiradev.core.helper.MonetaryFormatter
import com.github.oliveiradev.core.mapper.SingleMapper
import com.github.oliveiradev.search.presentation.ProductScreenModel

class SearchItemMapper(
    private val monetaryFormatter: MonetaryFormatter
) : SingleMapper<SearchItemResponse, ProductScreenModel> {

    override fun apply(from: SearchItemResponse): ProductScreenModel {
        return ProductScreenModel(
            from.title,
            monetaryFormatter.format(from.currencyId, from.price),
            formatInstallment(from.installments),
            from.thumbnail,
            from.condition,
            from.soldQuantity.toString(),
            from.availableQuantity.toString(),
            from.shipping.isFreeShipping
        )
    }

    private fun formatInstallment(installment: InstallmentsResponse?): String {
        return installment?.let {
            val quantity = it.quantity
            val formattedAmount = monetaryFormatter.format(it.currencyId, it.amount)
            "${quantity}x $formattedAmount"
        } ?: ""
    }
}