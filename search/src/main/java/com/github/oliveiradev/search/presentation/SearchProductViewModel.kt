package com.github.oliveiradev.search.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.github.oliveiradev.core.domain.SuspendedUseCase
import com.github.oliveiradev.core.helper.MainScopeViewModel
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class SearchProductViewModel(
    private val searchProductsUseCase: SuspendedUseCase<String, SearchScreenViewState>,
    coroutineContext: CoroutineContext
) : MainScopeViewModel(coroutineContext) {

    private val productsLiveData = MutableLiveData<SearchScreenViewState>()
    private val lastSearch = hashMapOf<String, SearchScreenViewState>()

    private fun publishProductState(state: SearchScreenViewState) {
        productsLiveData.postValue(state)
    }

    fun searchProducts(query: String) {
        lastSearch[query]?.run {
            publishProductState(this)
            return
        }

        scope.launch {
            productsLiveData.postValue(Loading)
            lastSearch.clear()
            val result = searchProductsUseCase.run(query)
            lastSearch[query] = result
            publishProductState(result)
        }
    }

    fun observeProducts(): LiveData<SearchScreenViewState> {
        return productsLiveData
    }
}