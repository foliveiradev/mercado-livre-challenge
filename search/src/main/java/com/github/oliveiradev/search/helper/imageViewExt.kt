package com.github.oliveiradev.search.helper

import android.widget.ImageView

fun ImageView.loadUrl(url: String) {
    GlideApp
        .with(context)
        .load(url)
        .fitCenter()
        .into(this)
}