package com.github.oliveiradev.search.domain

import com.github.oliveiradev.core.data.search.SearchProductGateway
import com.github.oliveiradev.core.data.search.SearchResponse
import com.github.oliveiradev.core.domain.SuspendedUseCase
import com.github.oliveiradev.core.mapper.SingleMapper
import com.github.oliveiradev.search.presentation.Content
import com.github.oliveiradev.search.presentation.Error
import com.github.oliveiradev.search.presentation.NoContent
import com.github.oliveiradev.search.presentation.SearchScreenModel
import com.github.oliveiradev.search.presentation.SearchScreenViewState
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

class SearchProductUseCase(
    private val searchProductGateway: SearchProductGateway,
    private val searchMapper: SingleMapper<SearchResponse, SearchScreenModel>
) : SuspendedUseCase<String, SearchScreenViewState> {

    override suspend fun run(params: String): SearchScreenViewState {
        return withContext(IO) {
            val result = searchProductGateway.searchProducts(params)
            result.result(
                { response -> checkContent(response) },
                { error -> Error(error) }
            ) as SearchScreenViewState
        }
    }

    private fun checkContent(response: SearchResponse): SearchScreenViewState {
        val itemsSize = response.paging.total
        return if (itemsSize == 0L) NoContent else Content(searchMapper.apply(response))
    }
}