package com.github.oliveiradev.search.injection

import com.github.oliveiradev.core.data.search.SearchItemResponse
import com.github.oliveiradev.core.data.search.SearchResponse
import com.github.oliveiradev.core.domain.SuspendedUseCase
import com.github.oliveiradev.core.mapper.SingleMapper
import com.github.oliveiradev.search.domain.SearchProductUseCase
import com.github.oliveiradev.search.mapper.SearchItemMapper
import com.github.oliveiradev.search.mapper.SearchMapper
import com.github.oliveiradev.search.presentation.ProductScreenModel
import com.github.oliveiradev.search.presentation.SearchProductViewModel
import com.github.oliveiradev.search.presentation.SearchScreenModel
import com.github.oliveiradev.search.presentation.SearchScreenViewState
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val searchModule = module {
    val searchMapperIdentifier = named<SearchMapper>()
    val productMapperIdentifier = named<ProductScreenModel>()

    factory<SingleMapper<SearchItemResponse, ProductScreenModel>>(productMapperIdentifier) {
        SearchItemMapper(get())
    }

    factory<SingleMapper<SearchResponse, SearchScreenModel>>(searchMapperIdentifier) {
        SearchMapper(get(productMapperIdentifier))
    }

    factory<SuspendedUseCase<String, SearchScreenViewState>> {
        SearchProductUseCase(get(), get(searchMapperIdentifier))
    }

    viewModel {
        SearchProductViewModel(get(), get())
    }
}