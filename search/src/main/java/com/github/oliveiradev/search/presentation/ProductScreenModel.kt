package com.github.oliveiradev.search.presentation

import android.os.Parcel
import android.os.Parcelable

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
data class ProductScreenModel(
    val title: String,
    val price: String,
    val installment: String,
    val thumbnail: String,
    val condition: String,
    val soldQuantity: String,
    val availableQuantity: String,
    val isFreeShipping: Boolean
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readByte() != 0.toByte()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.apply {
            writeString(title)
            writeString(price)
            writeString(installment)
            writeString(thumbnail)
            writeString(condition)
            writeString(soldQuantity)
            writeString(availableQuantity)
            writeByte(if (isFreeShipping) 1 else 0)
        }
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProductScreenModel> {
        override fun createFromParcel(parcel: Parcel): ProductScreenModel {
            return ProductScreenModel(parcel)
        }

        override fun newArray(size: Int): Array<ProductScreenModel?> {
            return arrayOfNulls(size)
        }
    }
}