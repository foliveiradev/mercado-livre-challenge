package com.github.oliveiradev.search.presentation

import android.os.Parcel
import android.os.Parcelable

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
data class SearchScreenModel(
    val totalResult: String,
    val products: List<ProductScreenModel>
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.createTypedArrayList(ProductScreenModel)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(totalResult)
        parcel.writeTypedList(products)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SearchScreenModel> {
        override fun createFromParcel(parcel: Parcel): SearchScreenModel {
            return SearchScreenModel(parcel)
        }

        override fun newArray(size: Int): Array<SearchScreenModel?> {
            return arrayOfNulls(size)
        }
    }
}