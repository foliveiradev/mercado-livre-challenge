package com.github.oliveiradev.search.presentation.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.github.oliveiradev.core.delegates.extraProvider
import com.github.oliveiradev.core.delegates.viewProvider
import com.github.oliveiradev.core.helper.enableBack
import com.github.oliveiradev.core.helper.toast
import com.github.oliveiradev.search.R
import com.github.oliveiradev.search.helper.loadUrl
import com.github.oliveiradev.search.presentation.ProductScreenModel

class ProductDetailActivity : AppCompatActivity() {

    private val product by extraProvider<ProductScreenModel>(PRODUCT_EXTRA)

    private val toolbar: Toolbar by viewProvider(R.id.toolbar)
    private val productImage: ImageView by viewProvider(R.id.product_thumb)
    private val productName: TextView by viewProvider(R.id.product_name)
    private val productPrice: TextView by viewProvider(R.id.product_price)
    private val productCondition: TextView by viewProvider(R.id.product_condition)
    private val productInstallment: TextView by viewProvider(R.id.product_installment)
    private val productAvailableQuantity: TextView by viewProvider(R.id.product_available_quantity)
    private val freeShippingText: TextView by viewProvider(R.id.product_shipping_type)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        setupView()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    private fun setupView() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            enableBack()
        }
        product?.let {
            bindProduct(it)
        } ?: toast(R.string.product_load_error)
    }

    private fun bindProduct(productScreenModel: ProductScreenModel) = with(productScreenModel) {
        productImage.loadUrl(thumbnail)
        productName.text = title
        productPrice.text = price
        productCondition.text = getString(
            R.string.sold_items_template,
            condition.toUpperCase(),
            soldQuantity
        )
        productInstallment.text = installment
        freeShippingText.visibility = if (isFreeShipping) View.VISIBLE else View.GONE
        productAvailableQuantity.text = getString(
            R.string.available_items_template,
            availableQuantity
        )
    }

    companion object {

        private const val PRODUCT_EXTRA = "PRODUCT_EXTRA"

        fun createIntent(context: Context, product: ProductScreenModel): Intent {
            return Intent(context, ProductDetailActivity::class.java)
                .putExtra(PRODUCT_EXTRA, product)
        }
    }
}