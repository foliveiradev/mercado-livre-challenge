package com.github.oliveiradev.search.presentation.custom_view

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.github.oliveiradev.core.delegates.viewProvider
import com.github.oliveiradev.search.R
import java.util.Timer
import java.util.TimerTask

class SingleSearchView : ConstraintLayout {

    private var onTextChangeListener: ((String) -> Unit)? = null
    private var onClearClickListener: (() -> Unit)? = null
    private var debounce = 0L
    private var timer: Timer = Timer()
    private val search: EditText by viewProvider(R.id.search)
    private val clearAction: ImageView by viewProvider(R.id.clear_action)
    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (s.isNullOrBlank()) return
            timer.cancel()
            emitText(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            changeClearActionVisibility(count)
        }
    }

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0) {
        applyDebounceTime(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        applyDebounceTime(context, attrs)
    }

    init {
        inflate(context, R.layout.view_single_search, this)
        background = ContextCompat.getDrawable(context, R.drawable.shape_rounded_white)
        setup()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        timer.cancel()
        search.removeTextChangedListener(textWatcher)
        onTextChangeListener = null
    }

    private fun applyDebounceTime(context: Context, attrs: AttributeSet?) {
        val typedArray = context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.SingleSearchView,
            0,
            0
        )
        try {
            debounce = typedArray.getInteger(
                R.styleable.SingleSearchView_debounce,
                0
            ).toLong()
        } finally {
            typedArray.recycle()
        }
    }

    private fun setup() {
        search.addTextChangedListener(textWatcher)
        clearAction.setOnClickListener {
            onClearClickListener?.invoke()
            search.setText("")
        }
    }

    private fun changeClearActionVisibility(count: Int) {
        if (count == 0) {
            clearAction.visibility = View.GONE
            search.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_search_black_24dp,
                0
            )
        } else {
            clearAction.visibility = View.VISIBLE
            search.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        }
    }

    private fun emitText(text: String) {
        timer = Timer()
        timer.schedule(
            object : TimerTask() {
                override fun run() {
                    onTextChangeListener?.invoke(text)
                }
            },
            debounce
        )
    }

    fun setDebounce(debounce: Long) {
        this.debounce = debounce
    }

    fun setOnTextChangeListener(onTextChangeListener: (String) -> Unit) {
        this.onTextChangeListener = onTextChangeListener
    }

    fun setOnClearClickListener(onClearClickListener: () -> Unit) {
        this.onClearClickListener = onClearClickListener
    }
}