package com.github.oliveiradev.search.presentation.list

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.annotation.VisibleForTesting
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.oliveiradev.core.delegates.viewProvider
import com.github.oliveiradev.core.helper.hideKeyboard
import com.github.oliveiradev.core.helper.toast
import com.github.oliveiradev.search.R
import com.github.oliveiradev.search.presentation.Content
import com.github.oliveiradev.search.presentation.Error
import com.github.oliveiradev.search.presentation.Loading
import com.github.oliveiradev.search.presentation.NoContent
import com.github.oliveiradev.search.presentation.ProductScreenModel
import com.github.oliveiradev.search.presentation.SearchProductViewModel
import com.github.oliveiradev.search.presentation.SearchScreenViewState
import com.github.oliveiradev.search.presentation.custom_view.SingleSearchView
import com.github.oliveiradev.search.presentation.detail.ProductDetailActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchProductActivity : AppCompatActivity() {

    private val search: SingleSearchView by viewProvider(R.id.product_search)
    private val progress: ProgressBar by viewProvider(R.id.progress)
    private val initialState: View by viewProvider(R.id.initial_state)
    private val emptyState: View by viewProvider(R.id.empty_state)
    private val totalFoundItems: TextView by viewProvider(R.id.total)
    private val products: RecyclerView by viewProvider(R.id.products)
    private val productsAdapter by lazy {
        SearchProductRecyclerAdapter { startDetailActivity(it) }
    }
    private val searchProductViewModel: SearchProductViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_product)
        searchProductViewModel.observeProducts().observe(this, Observer {
            processState(it)
        })
        setupView()
    }

    private fun setupView() {
        productsAdapter.setHasStableIds(true)
        products.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@SearchProductActivity)
            addItemDecoration(DividerItemDecoration(this@SearchProductActivity, VERTICAL))
            adapter = productsAdapter
        }

        search.apply {
            setOnTextChangeListener(searchProductViewModel::searchProducts)
            setOnClearClickListener(::onClearActionClick)
        }
    }

    private fun processState(state: SearchScreenViewState) {
        when (state) {
            Loading -> {
                initialState.visibility = View.GONE
                totalFoundItems.visibility = View.GONE
                progress.visibility = View.VISIBLE
            }

            NoContent -> {
                hideKeyboard()
                initialState.visibility = View.GONE
                progress.visibility = View.GONE
                emptyState.visibility = View.VISIBLE
                totalFoundItems.visibility = View.GONE
            }

            is Content -> {
                hideKeyboard()
                progress.visibility = View.GONE
                emptyState.visibility = View.GONE
                initialState.visibility = View.GONE
                totalFoundItems.visibility = View.VISIBLE
                totalFoundItems.text = state.content.totalResult
                productsAdapter.addAll(state.content.products)
            }

            is Error -> {
                hideKeyboard()
                initialState.visibility = View.VISIBLE
                progress.visibility = View.GONE
                totalFoundItems.visibility = View.GONE
                state.error.message?.let { toast(it) } ?: toast(R.string.unexpected_error)
            }
        }
    }

    private fun onClearActionClick() {
        hideKeyboard()
        totalFoundItems.apply {
            text = ""
            visibility = View.GONE
        }
        searchProductViewModel.cancel()
        productsAdapter.clear()
        emptyState.visibility = View.GONE
        initialState.visibility = View.VISIBLE
    }

    private fun startDetailActivity(product: ProductScreenModel) {
        startActivity(ProductDetailActivity.createIntent(this, product))
    }

    @VisibleForTesting
    fun setDebounceTime(debounce: Long) {
        search.setDebounce(debounce)
    }
}