package com.github.oliveiradev.search.mapper

import com.github.oliveiradev.core.data.search.SearchItemResponse
import com.github.oliveiradev.core.data.search.SearchResponse
import com.github.oliveiradev.core.mapper.SingleMapper
import com.github.oliveiradev.search.presentation.ProductScreenModel
import com.github.oliveiradev.search.presentation.SearchScreenModel

class SearchMapper(
    private val productMapper: SingleMapper<SearchItemResponse, ProductScreenModel>
) : SingleMapper<SearchResponse, SearchScreenModel> {

    override fun apply(from: SearchResponse): SearchScreenModel {
        return SearchScreenModel(
            "${from.paging.total} resultados",
            from.results.map(productMapper::apply)
        )
    }
}