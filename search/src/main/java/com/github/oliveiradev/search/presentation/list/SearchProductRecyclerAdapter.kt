package com.github.oliveiradev.search.presentation.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.github.oliveiradev.core.helper.ViewBinder
import com.github.oliveiradev.search.R
import com.github.oliveiradev.search.helper.loadUrl
import com.github.oliveiradev.search.presentation.ProductScreenModel

class SearchProductRecyclerAdapter(
    private val onItemClickListener: (ProductScreenModel) -> Unit
) : RecyclerView.Adapter<SearchProductRecyclerAdapter.ViewHolder>() {

    private val items: MutableList<ProductScreenModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_prodcut, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.apply {
            val item = items[position]
            bind(item)
            itemView.setOnClickListener { onItemClickListener.invoke(item) }
        }
    }

    override fun getItemId(position: Int): Long = items[position].hashCode().toLong()

    fun addAll(products: List<ProductScreenModel>) {
        val start = itemCount
        this.items.apply {
            clear()
            addAll(products)
        }

        notifyItemRangeInserted(start, products.size.minus(1))
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    fun getItem(position: Int): ProductScreenModel = items[position]

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view),
        ViewBinder<ProductScreenModel> {

        private val productName: TextView = itemView.findViewById(R.id.product_name)
        private val productInstallment: TextView = itemView.findViewById(R.id.product_installment)
        private val productPrice: TextView = itemView.findViewById(R.id.product_price)
        private val productThumb: ImageView = itemView.findViewById(R.id.product_thumb)

        override fun bind(item: ProductScreenModel) = with(item) {
            productName.text = title
            productInstallment.text = installment
            productPrice.text = price
            productThumb.loadUrl(thumbnail)
        }
    }
}