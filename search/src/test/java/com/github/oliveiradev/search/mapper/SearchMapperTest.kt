package com.github.oliveiradev.search.mapper

import com.github.oliveiradev.core.data.search.SearchItemResponse
import com.github.oliveiradev.core.mapper.SingleMapper
import com.github.oliveiradev.search.fixtures.createProductScreenModel
import com.github.oliveiradev.search.fixtures.createSearchItemResponse
import com.github.oliveiradev.search.fixtures.createSearchResponse
import com.github.oliveiradev.search.presentation.ProductScreenModel
import com.github.oliveiradev.search.presentation.SearchScreenModel
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class SearchMapperTest {

    private val productMapper: SingleMapper<SearchItemResponse, ProductScreenModel> = mock()
    private val fakeProductScreenModel = createProductScreenModel(false)
    private val fakeSearchResponse = createSearchResponse(createSearchItemResponse(2))
    private lateinit var searchMapper: SearchMapper

    @Before
    fun setup() {
        searchMapper = SearchMapper(productMapper)
    }

    @Test
    fun givenSearchResponse_whenMapping_shouldReturnSearchScreenModelInstance() {
        whenever(productMapper.apply(any())).thenReturn(fakeProductScreenModel)

        val result = searchMapper.apply(fakeSearchResponse)

        assertThat(result, instanceOf(SearchScreenModel::class.java))
    }

    @Test
    fun givenSearchResponseWith2SearchItems_whenMapping_shouldInvokeProductMapperTwice() {
        searchMapper.apply(fakeSearchResponse)

        verify(productMapper, times(2)).apply(any())
    }
}