package com.github.oliveiradev.search.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.github.oliveiradev.core.domain.SuspendedUseCase
import com.github.oliveiradev.search.fixtures.createSearchScreenModel
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.lang.Exception

class SearchProductViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()
    private val searchProductUseCase: SuspendedUseCase<String, SearchScreenViewState> = mock()
    private val fakeSearchScreenModel = createSearchScreenModel()
    private val fakeSearchScreenContent = Content(fakeSearchScreenModel)
    private lateinit var searchProductViewModel: SearchProductViewModel

    @Before
    fun setup() {
        searchProductViewModel = SearchProductViewModel(
            searchProductUseCase,
            Dispatchers.Unconfined
        )
    }

    @Test
    fun givenQuery_whenPerformSearchAtFirstTime_shouldInvokeUseCaseToPerformSearch() {
        runBlocking {
            val query = "mesa"

            searchProductViewModel.searchProducts(query)

            verify(searchProductUseCase).run(query)
        }
    }

    @Test
    fun givenValidQuery_whenSearchWitContent_shouldReturnContentState() {
        runBlocking {
            val query = "mesa"
            whenever(searchProductUseCase.run(query)).thenReturn(fakeSearchScreenContent)

            searchProductViewModel.searchProducts(query)
            val result = searchProductViewModel.observeProducts()

            assertEquals(result.value, fakeSearchScreenContent)
        }
    }

    @Test
    fun givenValidQuery_whenSearchWithoutContent_shouldReturnNoContentState() {
        runBlocking {
            val query = "dpaksldaskç"
            whenever(searchProductUseCase.run(query)).thenReturn(NoContent)

            searchProductViewModel.searchProducts(query)
            val result = searchProductViewModel.observeProducts()

            assertEquals(result.value, NoContent)
        }
    }

    @Test
    fun givenTheSameQuery_whenSearchAgain_shouldNotCallUseCaseTwice() {
        runBlocking {
            val query = "mesa"
            whenever(searchProductUseCase.run(query)).thenReturn(fakeSearchScreenContent)

            searchProductViewModel.searchProducts(query)
            searchProductViewModel.searchProducts(query)

            verify(searchProductUseCase).run(query)
        }
    }

    @Test
    fun givenUnexpectedError_whenSearch_shouldReturnErrorState() {
        runBlocking {
            val query = "mesa"
            val error = Error(Exception())
            whenever(searchProductUseCase.run(query)).thenReturn(error)

            searchProductViewModel.searchProducts(query)
            val result = searchProductViewModel.observeProducts()

            assertEquals(result.value, error)
        }
    }
}