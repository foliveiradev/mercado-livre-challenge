package com.github.oliveiradev.search.mapper

import com.github.oliveiradev.core.helper.MonetaryFormatter
import com.github.oliveiradev.search.fixtures.createInstallmentResponse
import com.github.oliveiradev.search.fixtures.createSearchItemResponse
import com.github.oliveiradev.search.presentation.ProductScreenModel
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.CoreMatchers.not
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class SearchItemMapperTest {

    private lateinit var searchItemMapper: SearchItemMapper

    @Before
    fun setup() {
        searchItemMapper = SearchItemMapper(MonetaryFormatter())
    }

    @Test
    fun givenSearchItemResponse_whenMapping_shouldReturnAProductScreenModelInstance() {
        val fakeSearchItem = createSearchItemResponse(createInstallmentResponse())
        val result = searchItemMapper.apply(fakeSearchItem)

        assertThat(result, instanceOf(ProductScreenModel::class.java))
    }

    @Test
    fun givenSearchItemResponseWithNullInstallment_whenMapping_shouldReturnEmptyInstallment() {
        val fakeSearchItem = createSearchItemResponse(null)
        val result = searchItemMapper.apply(fakeSearchItem)

        assertThat(result.installment, `is`(""))
    }

    @Test
    fun givenSearchItemResponseWithNonNullInstallment_whenMapping_shouldReturnEmptyInstallment() {
        val fakeSearchItem = createSearchItemResponse(createInstallmentResponse())
        val result = searchItemMapper.apply(fakeSearchItem)

        assertThat(result.installment, not(""))
    }
}