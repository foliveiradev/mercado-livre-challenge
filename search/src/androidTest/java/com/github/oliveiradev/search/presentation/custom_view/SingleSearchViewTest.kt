package com.github.oliveiradev.search.presentation.custom_view

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.github.oliveiradev.test_shared.BaseViewTest
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SingleSearchViewTest : BaseViewTest() {

    @Test
    fun givenEmptyText_whenShowInitialStateView_shouldShowHintSearchProduct() {
        searchView {
            launch()
            showHintText("Buscar produto")
        }
    }

    @Test
    fun givenSomeTextToSearchView_whenTypeText_shouldShowClearAction() {
        searchView {
            launch()
        } typeSomeSearch {
            showClearAction()
        }
    }

    @Test
    fun givenSomeTextToSearchView_whenUseTextChangeListener_shouldInvokeListenerAtLeastOnce() {
        searchView {
            launch(shouldSetTextChangeListener = true)
        } typeSomeSearch {
            textChangeListenerWasInvoked()
        }
    }

    @Test
    fun givenSomeTextToSearchView_whenClickClearAction_shouldShowEmptyEditText() {
        searchView {
            launch()
        } typeSomeSearch {
            clickClearAction {
                searchEditTextIsEmpty()
            }
        }
    }

    @Test
    fun givenSomeTextToSearchView_whenClickClearAction_shouldInvokeClearClickListenerOnce() {
        searchView {
            launch()
        } typeSomeSearch {
            clickClearAction {
                clearActionListenerWasInvoked()
            }
        }
    }
}