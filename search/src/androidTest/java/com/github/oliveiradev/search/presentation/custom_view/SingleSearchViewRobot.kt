package com.github.oliveiradev.search.presentation.custom_view

import android.content.Context
import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withHint
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import com.github.oliveiradev.search.R
import com.github.oliveiradev.test_shared.TestActivity
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify

fun SingleSearchViewTest.searchView(func: SingleSearchViewRobot.() -> Unit) =
    SingleSearchViewRobot(rule).apply(func)

class SingleSearchViewRobot(private val rule: ActivityTestRule<TestActivity>) {

    private val textChangeListener: (CharSequence) -> Unit = mock()
    private val clearTextClickListener: () -> Unit = mock()

    private fun launchView(view: (Context) -> View) {
        rule.activity?.setView(view)
    }

    fun launch(shouldSetTextChangeListener: Boolean = false) {
        launchView {
            SingleSearchView(it).apply {
                if (shouldSetTextChangeListener) setOnTextChangeListener(textChangeListener)
                setOnClearClickListener(clearTextClickListener)
            }
        }
    }

    fun showHintText(text: String) {
        onView(withId(R.id.search)).check(matches(withHint(text)))
    }

    fun showClearAction() {
        onView(withId(R.id.clear_action)).check(matches(isDisplayed()))
    }

    fun textChangeListenerWasInvoked() {
        verify(textChangeListener, atLeastOnce()).invoke(any())
    }

    fun searchEditTextIsEmpty() {
        onView(withId(R.id.search)).check(matches(withText("")))
    }

    fun clearActionListenerWasInvoked() {
        verify(clearTextClickListener).invoke()
    }

    infix fun typeSomeSearch(func: SingleSearchViewRobot.() -> Unit): SingleSearchViewRobot {
        onView(withId(R.id.search)).perform(typeText("Play"))
        return apply(func)
    }

    infix fun clickClearAction(func: SingleSearchViewRobot.() -> Unit): SingleSearchViewRobot {
        onView(withId(R.id.clear_action)).perform(click())
        return apply(func)
    }
}