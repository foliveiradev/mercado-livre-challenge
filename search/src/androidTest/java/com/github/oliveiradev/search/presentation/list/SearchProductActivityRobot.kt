package com.github.oliveiradev.search.presentation.list

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import com.github.oliveiradev.search.R
import com.github.oliveiradev.search.presentation.ProductScreenModel
import com.github.oliveiradev.test_shared.MockServer
import com.github.oliveiradev.test_shared.doWait
import com.github.oliveiradev.test_shared.withToastMatcher
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Description
import org.hamcrest.Matcher

fun SearchProductActivityTest.search(
    func: SearchProductActivityRobot.() -> Unit
) = SearchProductActivityRobot(server, rule).apply(func)

class SearchProductActivityRobot(
    private val server: MockServer,
    private val rule: ActivityTestRule<SearchProductActivity>
) {

    private val debounceTime = 500L
    private val termToSearch = "mesa"
    private lateinit var matcher: Matcher<Intent>

    fun launch() {
        rule.launchActivity(Intent())
        rule.activity.setDebounceTime(debounceTime)
    }

    fun createMatcher(className: String) {
        matcher = allOf(
            IntentMatchers.hasComponent(className)
        )
    }

    fun intendingMatcher() {
        val result = Instrumentation.ActivityResult(Activity.RESULT_OK, null)
        Intents.intending(matcher).respondWith(result)
    }

    fun matcherWasIntended() {
        Intents.intended(matcher)
    }

    fun showInitialMessages() {
        onView(withText("Como podemos te ajudar?")).check(matches(isDisplayed()))
        onView(withText("Nos diga o que procura na barra de busca")).check(matches(isDisplayed()))
    }

    fun showEmptyStateMessages() {
        doWait()
        onView(withText("Não achamos nada com essa descrição")).check(matches(isDisplayed()))
        onView(withText("Use a barra de busca para tentarmos novamente")).check(matches(isDisplayed()))
    }

    fun firstItemOfListContainsTheTermUsedInSearch() {
        doWait()
        onView(withId(R.id.products))
            .check(matches(atPosition(0, containsInTitle(termToSearch))))
    }

    fun showUnexpectedErrorMessage() {
        onView(withText("Não conseguimos processar sua busca, tente novamente mais tarde"))
            .inRoot(withToastMatcher())
            .check(matches(isDisplayed()))
    }

    infix fun performSearch(func: SearchProductActivityRobot.() -> Unit): SearchProductActivityRobot {
        server.newMockedCall("200_has_products.json", 200)
        onView(withId(R.id.search)).perform(typeText(termToSearch))
        doWait(debounceTime)
        return apply(func)
    }

    infix fun performSearchWithError(func: SearchProductActivityRobot.() -> Unit): SearchProductActivityRobot {
        server.newMockedCall("200_has_products.json", 500)
        onView(withId(R.id.search)).perform(typeText(termToSearch))
        doWait(debounceTime)
        return apply(func)
    }

    infix fun performInvalidSearch(func: SearchProductActivityRobot.() -> Unit): SearchProductActivityRobot {
        server.newMockedCall("200_has_no_products.json", 200)
        onView(withId(R.id.search)).perform(typeText("kjsdfk"))
        doWait(debounceTime)
        return apply(func)
    }

    infix fun clearSearch(func: SearchProductActivityRobot.() -> Unit): SearchProductActivityRobot {
        onView(withId(R.id.clear_action)).perform(click())
        return apply(func)
    }

    infix fun clickAtPosition0(func: SearchProductActivityRobot.() -> Unit): SearchProductActivityRobot {
        doWait()
        onView(withId(R.id.products))
            .perform(actionOnItemAtPosition<SearchProductRecyclerAdapter.ViewHolder>(0, click()))
        return apply(func)
    }

    private fun containsInTitle(term: String): (ProductScreenModel) -> Boolean {
        return { it.title.contains(term, ignoreCase = true) }
    }

    private fun atPosition(
        position: Int,
        evaluation: (ProductScreenModel) -> Boolean
    ): Matcher<View> {
        return object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("has item at position $position: ")
            }

            override fun matchesSafely(view: RecyclerView): Boolean {
                return evaluation.invoke(
                    (view.adapter as SearchProductRecyclerAdapter).getItem(
                        position
                    )
                )
            }
        }
    }
}