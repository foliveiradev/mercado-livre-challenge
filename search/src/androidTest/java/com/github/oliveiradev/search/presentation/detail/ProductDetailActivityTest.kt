package com.github.oliveiradev.search.presentation.detail

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.github.oliveiradev.test_shared.BaseActivityTest
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ProductDetailActivityTest : BaseActivityTest<ProductDetailActivity>(ProductDetailActivity::class) {

    @Test
    fun givenProductScreenModelWithoutFreeShipping_whenStartProductDetailScreen_shouldShowProductDetails() {
        detail {
            launch()
            assertDetailsWithoutFreeShipping()
        }
    }

    @Test
    fun givenProductScreenModel_whenStartProductDetailScreen_shouldShowProductDetails() {
        detail {
            launch(isFreeShipping = true)
            assertDetailsWithFreeShipping()
        }
    }

    @Test
    fun givenNullExtraProduct_whenStartProductDetailScreen_shouldShowErrorToast() {
        detail {
            launchNull()
            assertToastMessage()
        }
    }
}