package com.github.oliveiradev.search.presentation.detail

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import com.github.oliveiradev.search.R
import com.github.oliveiradev.search.fixtures.createProductScreenModel
import com.github.oliveiradev.test_shared.withToastMatcher
import org.hamcrest.CoreMatchers.not

fun ProductDetailActivityTest.detail(func: ProductDetailActivityRobot.() -> Unit): ProductDetailActivityRobot =
    ProductDetailActivityRobot(rule).apply(func)

class ProductDetailActivityRobot(private val rule: ActivityTestRule<ProductDetailActivity>) {

    fun launch(isFreeShipping: Boolean = false) {
        rule.launchActivity(Intent()
            .putExtra("PRODUCT_EXTRA", createProductScreenModel(isFreeShipping)))
    }

    fun launchNull() {
        rule.launchActivity(Intent())
    }

    fun assertDetailsWithoutFreeShipping() {
        listOf(
            R.id.product_thumb,
            R.id.product_name,
            R.id.product_price,
            R.id.product_available_quantity,
            R.id.product_installment,
            R.id.product_condition
        ).forEach {
            onView(withId(it)).check(matches(isDisplayed()))
        }
        onView(withId(R.id.product_shipping_type)).check(matches(not(isDisplayed())))
    }

    fun assertDetailsWithFreeShipping() {
        listOf(
            R.id.product_thumb,
            R.id.product_name,
            R.id.product_price,
            R.id.product_available_quantity,
            R.id.product_installment,
            R.id.product_condition,
            R.id.product_shipping_type
        ).forEach {
            onView(withId(it)).check(matches(isDisplayed()))
        }
    }

    fun assertToastMessage() {
        onView(withText("Tivemos um problema ao carregar seu produto"))
            .inRoot(withToastMatcher())
            .check(matches(isDisplayed()))
    }
}