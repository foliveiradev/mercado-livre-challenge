package com.github.oliveiradev.search.presentation.list

import androidx.test.platform.app.InstrumentationRegistry
import com.github.oliveiradev.core.data.injection.dataModule
import com.github.oliveiradev.core.data.injection.dataModuleTest
import com.github.oliveiradev.core.injection.coreModule
import com.github.oliveiradev.core.injection.coreModuleTest
import com.github.oliveiradev.search.fixtures.createProductScreenModel
import com.github.oliveiradev.search.injection.searchModule
import com.github.oliveiradev.search.presentation.detail.ProductDetailActivity
import com.github.oliveiradev.test_shared.BaseActivityTest
import com.github.oliveiradev.test_shared.MockServer
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest

class SearchProductActivityTest :
    BaseActivityTest<SearchProductActivity>(SearchProductActivity::class), KoinTest {

    val server by lazy { MockServer() }

    @Before
    fun setup() {
        server.start()
        startKoin {
            androidLogger()
            androidContext(InstrumentationRegistry.getInstrumentation().context)
            modules(listOf(dataModule, dataModuleTest(server.url), coreModule, coreModuleTest, searchModule))
        }
    }

    @After
    fun tearDown() {
        server.shutdown()
        stopKoin()
    }

    @Test
    fun givenInitialState_whenStartSearchScreen_shouldShowInitialStateMessages() {
        search {
            launch()
            showInitialMessages()
        }
    }

    @Test
    fun givenValidTypedText_whenSearch_shouldPopulateProductList() {
        search {
            launch()
        } performSearch {
            firstItemOfListContainsTheTermUsedInSearch()
        }
    }

    @Test
    fun givenInvalidTypedText_whenSearch_shouldShowEmptyStateMessages() {
        search {
            launch()
        } performInvalidSearch {
            showEmptyStateMessages()
        }
    }

    @Test
    fun givenEmptyState_whenSearchAndClear_shouldShowInitialStateMessagesAgain() {
        search {
            launch()
        } performInvalidSearch {
            clearSearch {
                showInitialMessages()
            }
        }
    }

    @Test
    fun givenValidSearch_whenClickAtPosition0_shouldIntentProductDetail() {
        search {
            createMatcher(ProductDetailActivity::class.java.name)
            launch()
            intendingMatcher()
        } performSearch {
            clickAtPosition0 {
                matcherWasIntended()
            }
        }
    }
}