package com.github.oliveiradev.core.helper

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import java.lang.IllegalArgumentException

class MonetaryFormatterTest {

    private lateinit var monetaryFormatter: MonetaryFormatter

    @Before
    fun setup() {
        monetaryFormatter = MonetaryFormatter()
    }

    @Test
    fun givenValidParameters_whenFormat_shouldShowFormattedMonetaryValue() {
        val currency = "USD"
        val value = 50.toBigDecimal()

        val result = monetaryFormatter.format(currency, value)

        assertThat(result, equalTo("$ 50"))
    }

    @Test(expected = IllegalArgumentException::class)
    fun givenNonValidParameters_whenFormat_shouldThrowIllegalArgumentException() {
        val currency = "askdjdkas"
        val value = 50.toBigDecimal()

        val result = monetaryFormatter.format(currency, value)

        assertThat(result, equalTo("$ 50"))
    }
}