package com.github.oliveiradev.core.data.search.fixtures

import com.github.oliveiradev.core.data.search.PagingResponse
import com.github.oliveiradev.core.data.search.SearchResponse
import okhttp3.MediaType
import okhttp3.ResponseBody

fun createSearchResponse(): SearchResponse {
    return SearchResponse(
        "query",
        "siteId",
        PagingResponse(1, 0, 50),
        emptyList()
    )
}

fun createErrorResponseBody(): ResponseBody {
    return ResponseBody.create(MediaType.parse("application/json"), "{error: 'server unavailable'}")
}