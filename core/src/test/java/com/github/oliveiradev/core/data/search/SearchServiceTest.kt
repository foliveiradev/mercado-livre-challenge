package com.github.oliveiradev.core.data.search

import com.github.oliveiradev.core.data.search.fixtures.createErrorResponseBody
import com.github.oliveiradev.core.data.search.fixtures.createSearchResponse
import com.github.oliveiradev.core.helper.Result
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import retrofit2.Response
import java.net.SocketTimeoutException

class SearchServiceTest {

    private val searchAPI: SearchAPI = mock()
    private lateinit var searchService: SearchService
    private val fakeSearchResponse = createSearchResponse()
    private val fakeErrorResponseBody = createErrorResponseBody()

    @Before
    fun setup() {
        searchService = SearchService(searchAPI)
    }


    @Test
    fun giveValidAPI_whenCallSuccess_shouldReturnASuccessResult() {
        runBlocking {
            whenever(searchAPI.searchProducts(any())).thenReturn(Response.success(fakeSearchResponse))

            val result = searchService.searchProducts("mesa")

            assertTrue(result.isSuccess)
        }
    }

    @Test
    fun giveValidAPI_whenCallError_shouldReturnAErrorResult() {
        runBlocking {
            whenever(searchAPI.searchProducts(any())).thenReturn(Response.error(400, fakeErrorResponseBody))

            val result = searchService.searchProducts("mesa")

            assertTrue(result.isError)
        }
    }

    @Test
    fun giveValidAPI_whenCallError_shouldReturnASpecifErrorMessage() {
        runBlocking {
            whenever(searchAPI.searchProducts(any())).thenReturn(Response.error(400, fakeErrorResponseBody))

            val result = searchService.searchProducts("mesa")
            val error = result as Result.Error

            assertThat(error.b.message, equalTo("Não conseguimos processar sua busca, tente novamente mais tarde"))
        }
    }

    @Test
    fun giveValidAPI_whenUnexpectedErrorIsThrown_shouldReturnResultError() {
        runBlocking {
            whenever(searchAPI.searchProducts(any())).doAnswer { throw SocketTimeoutException() }

            val result = searchService.searchProducts("mesa")

            assertTrue(result.isError)
        }
    }

    @Test
    fun giveValidAPI_whenUnexpectedErrorIsThrown_shouldReturnASpecifErrorMessage() {
        runBlocking {
            whenever(searchAPI.searchProducts(any())).doAnswer { throw SocketTimeoutException() }

            val result = searchService.searchProducts("mesa")
            val error = result as Result.Error

            assertThat(error.b.message, equalTo( "Tivemos problemas ao se comunicar com o servidor, tente novamente mais tarde."))
        }
    }
}