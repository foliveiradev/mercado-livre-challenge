package com.github.oliveiradev.core.helper

import androidx.appcompat.app.ActionBar

fun ActionBar?.enableBack() {
    if (this == null) return
    setDisplayHomeAsUpEnabled(true)
    setDisplayShowHomeEnabled(true)
}