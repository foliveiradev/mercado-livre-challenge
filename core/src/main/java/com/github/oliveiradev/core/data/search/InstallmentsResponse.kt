package com.github.oliveiradev.core.data.search

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class InstallmentsResponse(
    val quantity: Int?,
    val amount: BigDecimal,
    val rate: Int,
    @SerializedName("currency_id") val currencyId: String
)