package com.github.oliveiradev.core.helper

interface ViewBinder<in T> {
    fun bind(item: T)
}