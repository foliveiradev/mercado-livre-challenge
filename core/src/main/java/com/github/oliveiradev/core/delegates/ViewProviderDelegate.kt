package com.github.oliveiradev.core.delegates

import android.app.Activity
import android.view.View
import androidx.annotation.IdRes
import kotlin.reflect.KProperty

class ViewProviderDelegate<T : View>(@IdRes val id: Int) {

    operator fun getValue(thisRef: Activity, property: KProperty<*>): T {
        return thisRef.findViewById(id)
    }

    operator fun getValue(thisRef: View, property: KProperty<*>): T {
        return thisRef.findViewById(id)
    }
}

fun <T : View> viewProvider(@IdRes idRes: Int) = ViewProviderDelegate<T>(idRes)