package com.github.oliveiradev.core.data.search

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class SearchItemResponse(
    val title: String,
    val thumbnail: String,
    val price: BigDecimal,
    @SerializedName("currency_id") val currencyId: String,
    val installments: InstallmentsResponse?,
    val condition: String,
    @SerializedName("sold_quantity") val soldQuantity: Int,
    @SerializedName("available_quantity") val availableQuantity: Int,
    val shipping: ShippingResponse
)