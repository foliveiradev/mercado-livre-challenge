package com.github.oliveiradev.core.data.search

import com.github.oliveiradev.core.data.shared.ErrorProvider
import com.github.oliveiradev.core.helper.Result
import timber.log.Timber

internal class SearchService(private val searchAPI: SearchAPI) : SearchProductGateway {

    override suspend fun searchProducts(query: String): SearchResult {
        return try {
            val result = searchAPI.searchProducts(query)
            when (result.isSuccessful) {
                true -> {
                    val body = result.body()
                    body?.let { Result.Success(it) } ?: Result.Error(unexpectedBody())
                }
                false -> {
                    val throwable = unexpectedBody()
                    Result.Error(throwable)
                }
            }
        } catch (e: Throwable) {
            Timber.e(e)
            Result.Error(ErrorProvider.provide(e))
        }
    }

    private fun unexpectedBody(): Throwable {
        val throwable = IllegalStateException(
            "Não conseguimos processar sua busca, tente novamente mais tarde"
        )
        Timber.e(throwable)
        return throwable
    }
}