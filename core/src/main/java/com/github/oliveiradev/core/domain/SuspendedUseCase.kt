package com.github.oliveiradev.core.domain

interface SuspendedUseCase<in Param, out Result> {

    suspend fun run(params: Param): Result
}