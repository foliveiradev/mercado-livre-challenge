package com.github.oliveiradev.core.helper

sealed class Result<out S, out E> {

    data class Success<out S>(val a: S) : Result<S, Nothing>()
    data class Error<out E>(val b: E) : Result<Nothing, E>()

    val isSuccess get() = this is Success<S>

    val isError get() = this is Error<E>

    fun result(fnS: (S) -> Any, fnE: (E) -> Any): Any =
        when (this) {
            is Success -> fnS(a)
            is Error -> fnE(b)
        }
}