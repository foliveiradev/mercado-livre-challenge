package com.github.oliveiradev.core.helper

import java.math.BigDecimal
import java.text.NumberFormat
import java.util.Currency

class MonetaryFormatter {

    fun format(currency: String, value: BigDecimal): String {
        val numberFormat = NumberFormat.getInstance()
        val currencyInstance = Currency.getInstance(currency)
        numberFormat.currency = currencyInstance
        return "${currencyInstance.symbol} ${numberFormat.format(value)}"
    }
}