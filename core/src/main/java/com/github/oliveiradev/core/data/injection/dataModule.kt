package com.github.oliveiradev.core.data.injection

import com.github.oliveiradev.core.BuildConfig
import com.github.oliveiradev.core.data.search.SearchAPI
import com.github.oliveiradev.core.data.search.SearchProductGateway
import com.github.oliveiradev.core.data.search.SearchService
import com.github.oliveiradev.core.data.shared.RetrofitBuilder
import com.github.oliveiradev.core.data.shared.RetrofitFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module

val baseUrl = named("BASE_URL")

val dataModule = module {

    single(baseUrl) {
        BuildConfig.BASE_URL
    }

    single<OkHttpClient> {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            .build()
    }

    single {
        RetrofitBuilder.createRetrofit(get(baseUrl), get())
    }

    factory<SearchAPI> { RetrofitFactory.create(get()) }

    factory<SearchProductGateway> { SearchService(get()) }
}

fun dataModuleTest(url: String) = module {

    single(baseUrl, override = true) {
        url
    }
}