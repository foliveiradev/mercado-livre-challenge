package com.github.oliveiradev.core.data.shared

import java.net.SocketTimeoutException
import java.net.UnknownHostException

object ErrorProvider {

    fun provide(throwable: Throwable): Throwable {
        return when (throwable) {
            is SocketTimeoutException -> SocketTimeoutException(
                "Tivemos problemas ao se comunicar com o servidor, tente novamente mais tarde."
            )

            is UnknownHostException -> UnknownHostException(
                "Verifique sua conexão com a internet e tente novamente"
            )

            else -> IllegalStateException(
                "Não conseguimos processar sua busca, tente novamente mais tarde"
            )
        }
    }
}