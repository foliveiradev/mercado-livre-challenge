package com.github.oliveiradev.core.helper

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelChildren
import kotlin.coroutines.CoroutineContext

abstract class MainScopeViewModel(coroutineContext: CoroutineContext) : ViewModel() {

    private val job = Job()
    val scope = CoroutineScope(coroutineContext + job)

    fun cancel() {
        scope.coroutineContext.cancelChildren()
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}