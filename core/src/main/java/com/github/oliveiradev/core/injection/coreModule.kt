package com.github.oliveiradev.core.injection

import com.github.oliveiradev.core.helper.MonetaryFormatter
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module
import kotlin.coroutines.CoroutineContext

val coreModule = module {

    single { MonetaryFormatter() }

    factory<CoroutineContext> { Dispatchers.Main }
}

val coreModuleTest = module {

    factory<CoroutineContext>(override = true) { Dispatchers.Unconfined }
}