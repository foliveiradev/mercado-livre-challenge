package com.github.oliveiradev.core.mapper

interface SingleMapper<in P, out R> {

    fun apply(from: P): R
}