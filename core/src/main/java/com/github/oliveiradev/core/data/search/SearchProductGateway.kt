package com.github.oliveiradev.core.data.search

import com.github.oliveiradev.core.helper.Result

typealias SearchResult = Result<SearchResponse, Throwable>

interface SearchProductGateway {

    suspend fun searchProducts(query: String): SearchResult
}