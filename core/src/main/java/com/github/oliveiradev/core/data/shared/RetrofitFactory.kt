package com.github.oliveiradev.core.data.shared

import retrofit2.Retrofit

object RetrofitFactory {

    inline fun <reified T> create(
        retrofit: Retrofit
    ): T {
        return retrofit.create(T::class.java)
    }
}