package com.github.oliveiradev.core.data.search

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchAPI {

    @GET("sites/MLU/search")
    suspend fun searchProducts(@Query("q") query: String): Response<SearchResponse>
}