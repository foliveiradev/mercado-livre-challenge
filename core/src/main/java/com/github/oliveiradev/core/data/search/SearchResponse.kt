package com.github.oliveiradev.core.data.search

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    val query: String,
    @SerializedName("site_id") val siteId: String,
    val paging: PagingResponse,
    val results: List<SearchItemResponse>
)