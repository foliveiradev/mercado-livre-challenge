package com.github.oliveiradev.core.data.search

data class PagingResponse(
    val total: Long,
    val offset: Int,
    val limit: Int
)