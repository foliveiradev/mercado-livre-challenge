package com.github.oliveiradev.core.delegates

import android.app.Activity
import kotlin.reflect.KProperty

@Suppress("UNCHECKED_CAST")
class ExtraProviderDelegate<T>(private val identifier: String) {

    operator fun getValue(thisRef: Activity, property: KProperty<*>): T? {
        return thisRef.intent.extras?.get(identifier) as? T
    }
}

fun <T> extraProvider(identifier: String) = ExtraProviderDelegate<T>(identifier)