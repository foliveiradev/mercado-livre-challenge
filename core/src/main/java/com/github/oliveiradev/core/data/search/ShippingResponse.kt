package com.github.oliveiradev.core.data.search

import com.google.gson.annotations.SerializedName

data class ShippingResponse(
    @SerializedName("free_shipping") val isFreeShipping: Boolean
)