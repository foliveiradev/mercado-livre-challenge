package com.github.oliveiradev.test_shared

import android.app.Activity
import androidx.test.espresso.intent.rule.IntentsTestRule
import org.junit.Rule
import kotlin.reflect.KClass

open class BaseActivityTest<AC : Activity>(activityClass: KClass<AC>) {

    @Rule
    @JvmField
    val rule = IntentsTestRule(activityClass.java, true, false)
}