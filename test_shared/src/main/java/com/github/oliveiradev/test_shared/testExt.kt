package com.github.oliveiradev.test_shared

import android.view.View
import android.view.WindowManager
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Root
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.matcher.BoundedMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher

fun withToastMatcher(): TypeSafeMatcher<Root> {

    return object : TypeSafeMatcher<Root>() {
        override fun describeTo(description: Description?) {
            description?.appendText("is toast")
        }

        override fun matchesSafely(item: Root?): Boolean {
            val type = item?.windowLayoutParams?.get()?.type
            if (type == WindowManager.LayoutParams.TYPE_TOAST) {
                val windowToken = item.decorView.windowToken
                val appToken = item.decorView.applicationWindowToken
                if (windowToken === appToken) {
                    return true
                }
            }

            return false
        }
    }
}

fun doWait(time: Long = 1000) {
    Thread.sleep(time)
}

private fun ViewInteraction.sizeIs(size: Int): Matcher<View> {
    return object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
        override fun describeTo(description: Description) {
            description.appendText("the size is $size")
        }

        override fun matchesSafely(view: RecyclerView): Boolean {
            return size == view.adapter?.itemCount ?: 0
        }
    }
}