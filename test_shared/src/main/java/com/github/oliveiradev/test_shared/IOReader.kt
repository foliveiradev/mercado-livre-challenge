package com.github.oliveiradev.test_shared

import java.io.InputStream

object IOReader {

    fun read(inputStream: InputStream): String {
        return inputStream.bufferedReader().use { it.readText() }
    }
}