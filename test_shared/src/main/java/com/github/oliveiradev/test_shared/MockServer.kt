package com.github.oliveiradev.test_shared

import androidx.test.platform.app.InstrumentationRegistry
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import java.io.IOException
import java.io.InputStream

class MockServer {

    private val mockWebServer = MockWebServer()
    val url by lazy { mockWebServer.url("/").toString() }

    fun start() {
        mockWebServer.start()
    }

    fun shutdown() {
        mockWebServer.shutdown()
    }

    fun newMockedCall(fixturePath: String, code: Int, useEmptyBody: Boolean = false) {
        mockWebServer.enqueue(createResponse(readFixture(fixturePath), code, useEmptyBody))
    }

    private fun createResponse(body: String, code: Int, useEmptyBody: Boolean): MockResponse {
        return MockResponse()
            .setBody(if (useEmptyBody) EMPTY_BODY else body)
            .setResponseCode(code)
    }

    private fun readFixture(fixturePath: String): String {
        try {
            return IOReader.read(open("$FIXTURE_ROOT_PATH$fixturePath"))
        } catch (e: IOException) {
            throw RuntimeException("Failed to read asset with path $fixturePath", e)
        }
    }

    @Throws(IOException::class)
    private fun open(path: String): InputStream {
        return InstrumentationRegistry.getInstrumentation().context.assets.open(path)
    }

    companion object {

        private const val FIXTURE_ROOT_PATH = "fixtures/"
        private const val EMPTY_BODY = ""
    }
}