package com.github.oliveiradev.test_shared

import android.content.Intent
import org.junit.Before

open class BaseViewTest : BaseActivityTest<TestActivity>(TestActivity::class) {

    @Before
    fun launchActivity() {
        rule.launchActivity(Intent())
    }
}