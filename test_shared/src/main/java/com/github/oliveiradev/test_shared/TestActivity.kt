package com.github.oliveiradev.test_shared

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity

class TestActivity : AppCompatActivity() {

    fun setView(view: (Context) -> View) = runOnUiThread {
        val rootView: ViewGroup = findViewById(android.R.id.content)
        rootView.addView(view.invoke(this))
    }
}
